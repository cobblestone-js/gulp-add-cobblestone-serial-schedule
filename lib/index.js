var dotProp = require("dot-prop");
var merge = require("merge");
var moment = require("moment");
var through = require("through2");

module.exports = function(params)
{
    // Normalize the parameters.
    params = params || {};
    params.scheduleProperty = params.scheduleProperty || "data.schedule";
    params.addProperties = params.addProperties || {};
    params.skipIf = params.skipIf || function() { return false; };
    params.daysBetween = params.daysBetween || 7;
    params.pathOffset = params.pathOffset || -1;
    params.pathPattern = params.pathPattern || /^.*\w+-(\d+)/;

    // Make sure we have some properties.
    if (!params.start) {
        throw new Error("start is required");
    }

    // Create a stream to manipulate the files based on the filename.
    var transform = through.obj(
        function(file, encoding, callback)
        {
            // See if the path matches the pattern, if it doesn't, then we just
            // pass it along untouched.
            var m = file.path.match(params.pathPattern);

            if (!m) return callback(null, file);

            // Check to see if we should skip this file.
            if (params.skipIf(file)) return callback(null, file);

            // Figure out the number of periods we want to use. This defaults to
            // 1 so `chapter-01.txt` will result in 0 while `chapter-10.txt`
            // will be 9.
            var index = parseInt(m[1]) + params.pathOffset;

            // Figure out the schedule. This is the starting date with the
            // number of periods (based on days) after that.
            var days = index * params.daysBetween;
            var start = moment(params.start);
            var scheduleDate = start.add(days, "days");

            // Get the schedule metadata, add our schedule, and then put it back.
            var scheduleList = dotProp.get(file, params.scheduleProperty) || [];
            var schedule = merge(
                { scheduleDate: scheduleDate.format("YYYY-MM-DD") },
                params.addProperties);

            scheduleList.push(schedule);
            dotProp.set(file, params.scheduleProperty, scheduleList);

            // We are done with this file.
            callback(null, file);
        });

    return transform;
}
